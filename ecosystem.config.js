module.exports = {
  apps: [
    {
      name: 'backend',
      script: 'npm run start',
      exec_mode: 'fork',
    },
  ],
};
