const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const cors = require('cors');
const routesV1 = require('./routes/v1');
const routesV2 = require('./routes/v2');

dotenv.config();

const PORT = process.env.PORT || 4000;

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

app.use(cors({ origin: '*', credentials: true }));

routesV1(app);
routesV2(app);

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
