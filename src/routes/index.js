const conexion = require('../sql/conexion');

const routes = (app) => {
  // Route
  app.get('/', (req, res) => {
    res.send('Welcome to my API');
  });

  // Users
  app.get('/list_users', (req, res) => {
    const sql = 'SELECT * FROM users';
    conexion.query(sql, (error, results) => {
      if (error) throw error;
      if (results.length > 0) {
        res.json(results);
      } else {
        res.send('Not results');
      }
    });
  });

  app.get('/user/:id', (req, res) => {
    const { id } = req.params;
    const sql = `SELECT * FROM users WHERE user = '${id}'`;
    conexion.query(sql, (error, result) => {
      if (error) throw error;
      if (result.length > 0) {
        res.json(result);
      } else {
        res.send('Not results');
      }
    });
  });

  app.post('/add_user', (req, res) => {
    const sql = 'INSERT INTO users SET ?';
    const userObj = {
      user: req.body.user,
      contactName: req.body.contactName,
      email: req.body.email,
      phone: req.body.phone,
      provincia: req.body.provincia,
      pais: req.body.pais,
      typeUser: req.body.typeUser,
      companyName: req.body.contactName,
      idCompany: req.body.idCompany,
      password: req.body.password,
    };
    conexion.query(sql, userObj, (error) => {
      if (error) throw error;
      res.send('User Create!');
    });
  });

  app.put('/update_user/:id', (req, res) => {
    const { id } = req.params;
    let queryName = '';
    let queryEmail = '';
    let queryPhone = '';
    let queryProvincia = '';
    let queryCountry = '';
    let queryTypeUser = '';
    let queryCName = '';
    let QueryIdCompany = '';
    let queryPassword = '';

    if (req.body.contactName && req.body.contactName.length > 0) {
      queryName = `_contactName = '${req.body.contactName}'?`;
    }
    if (req.body.email && req.body.email.length > 0) {
      queryEmail = `_email = '${req.body.email}'?`;
    }
    if (req.body.phone && req.body.phone.length > 0) {
      queryPhone = `_phone = '${req.body.phone}'?`;
    }
    if (req.body.provincia && req.body.provincia.length > 0) {
      queryProvincia = `_provincia = '${req.body.provincia}'?`;
    }
    if (req.body.pais && req.body.pais.length > 0) {
      queryCountry = `_pais = '${req.body.pais}'?`;
    }
    if (req.body.typeUser && req.body.typeUser.length > 0) {
      queryTypeUser = `_typeUser = '${req.body.typeUser}'?`;
    }
    if (req.body.companyName && req.body.companyName.length > 0) {
      queryCName = `_companyName = '${req.body.companyName}'?`;
    }
    if (req.body.idCompany && req.body.idCompany.length > 0) {
      QueryIdCompany = `_idCompany = '${req.body.idCompany}'?`;
    }
    if (req.body.password && req.body.password.length > 0) {
      queryPassword = `_password = '${req.body.password}'?`;
    }

    const queryVariables = `${queryName}${queryEmail}${queryPhone}${queryProvincia}${queryCountry}${queryTypeUser}${queryCName}${QueryIdCompany}${queryPassword}`;

    const subresult1 = queryVariables.replace(/\?_/g, ',');
    const subresult2 = subresult1.replace('?', '');
    const subresult3 = subresult2.replace('_', '');

    const sql = `UPDATE users SET ${subresult3} WHERE user = '${id}'`;

    conexion.query(sql, (error) => {
      if (error) throw error;
      res.send('User Updated!');
    });
  });

  app.delete('/delete_user/:id', (req, res) => {
    const { id } = req.params;
    const sql = `DELETE FROM users WHERE user = ${id}`;
    conexion.query(sql, (error) => {
      if (error) throw error;
      res.send('Delete User!');
    });
  });
};

module.exports = routes;
