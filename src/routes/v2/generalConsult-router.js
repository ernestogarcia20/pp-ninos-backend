const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const generalConsultController = require('../../controllers/v2/generalConsult-controller');

const router = express.Router();

router.post(
  '/consult-SeletOption',
  generalConsultController.getConsultSeletOption
);
router.post(
  '/multiTable-consulte',
  generalConsultController.getMultiTableConsulte
);

router.post('/create', isAuth, generalConsultController.createData);

router.post('/updata', isAuth, generalConsultController.editData);
router.post('/updata-numbersPublic', generalConsultController.updata);
router.post('/delete', isAuth, generalConsultController.deleteData);
router.post('/get', isAuth, generalConsultController.getData);
router.post('/get-all', generalConsultController.getAllData);
router.post('/click', generalConsultController.clicksPublications);
router.post('/getClickPerPost', generalConsultController.getClicksPerPost);
router.post(
  '/getClicks',
  generalConsultController.generalGetClicksPublications
);
router.post('/clickComponent', generalConsultController.clicksComponent);
router.get('/ispAsigna', generalConsultController.ispAsigna);

module.exports = router;
