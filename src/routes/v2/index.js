const generalConsultRouter = require('./generalConsult-router');
const subCategoriesRouter = require('./subCategories-router');
const categoriesRouter = require('./categories-router');

module.exports = (app) => {
  app.use('/api/v2/generalConsult', generalConsultRouter);
  app.use('/api/v2/subCategoties', subCategoriesRouter);
  app.use('/api/v2/categories', categoriesRouter);
};
