const express = require('express');
const categoriesController = require('../../controllers/v2/categories-controller');

const router = express.Router();

router.post('/get-name', categoriesController.getCategorieForName);

module.exports = router;
