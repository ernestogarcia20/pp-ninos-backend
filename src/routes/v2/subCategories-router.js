const express = require('express');
const subCategoriesController = require('../../controllers/v2/subCategories-controller');

const router = express.Router();

router.post(
  '/subCategoriesXCategories',
  subCategoriesController.consulteSubCategoriesFromCategories
);

module.exports = router;
