const express = require('express');
const utilsController = require('../../controllers/v1/utils-controller');

const router = express.Router();

router.get('/generator', utilsController.keyGenerator);

module.exports = router;
