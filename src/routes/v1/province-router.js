const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const provincesController = require('../../controllers/v1/province-controller');

const router = express.Router();

router.post('/create', isAuth, provincesController.createProvince);
router.post('/delete', isAuth, provincesController.deleteProvince);
router.post('/update', isAuth, provincesController.updateProvince);
router.post('/get', isAuth, provincesController.getProvince);
router.get('/get-all', provincesController.getProvinces);
router.get('/consultes', provincesController.consulteProvinces);
router.post(
  '/get-consultes-for-country',
  provincesController.consulteProvinceForCountry
);

module.exports = router;
