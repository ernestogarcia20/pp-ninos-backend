const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersByCenFacilitiesController = require('../../controllers/v1/filtersByCenFacility-controller');

const router = express.Router();

router.post(
  '/create',
  isAuth,
  filtersByCenFacilitiesController.createFiltersByCenFacility
);
router.post(
  '/delete',
  isAuth,
  filtersByCenFacilitiesController.deleteFiltersByCenFacility
);
router.post(
  '/update',
  isAuth,
  filtersByCenFacilitiesController.updateFiltersByCenFacility
);
router.post(
  '/get',
  isAuth,
  filtersByCenFacilitiesController.getFiltersByCenFacility
);
router.get(
  '/get-all',
  filtersByCenFacilitiesController.getFiltersByCenFacilities
);
router.get(
  '/consulte',
  filtersByCenFacilitiesController.consulteFiltersByCenFacilities
);

module.exports = router;
