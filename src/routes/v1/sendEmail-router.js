const express = require('express');
const sendEmail = require('../../controllers/v1/sendEmail-controller');

const router = express.Router();

router.post('/sendEmailCentroAyuda', sendEmail.sendEmailCentroAyuda);
router.post('/sendEmailComment', sendEmail.sendEmailComment);

module.exports = router;
