const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const commentNewsController = require('../../controllers/v1/commentNews-controller');

const router = express.Router();

router.post('/create', commentNewsController.createCommentNews);
router.post('/delete', isAuth, commentNewsController.deleteCommentNews);
router.post('/update', isAuth, commentNewsController.updateCommentNews);
router.post('/get', commentNewsController.getCommentNew);
router.post('/get-all', commentNewsController.getCommentsNews);

module.exports = router;
