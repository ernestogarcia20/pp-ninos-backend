const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const planController = require('../../controllers/v1/plan-controller');

const router = express.Router();

router.post('/create', isAuth, planController.createPlan);
router.post('/delete', isAuth, planController.deletePlan);
router.post('/update', isAuth, planController.updatePlan);
router.post('/get', isAuth, planController.getPlan);
router.get('/get-all', planController.getPlans);
router.get('/consulte', planController.consultePlans);

module.exports = router;
