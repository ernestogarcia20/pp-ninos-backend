const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersBySpecialistsController = require('../../controllers/v1/filtersBySpecialist-controller');

const router = express.Router();

router.post(
  '/create',
  isAuth,
  filtersBySpecialistsController.createFiltersBySpecialist
);
router.post(
  '/delete',
  isAuth,
  filtersBySpecialistsController.deleteFiltersBySpecialist
);
router.post(
  '/update',
  isAuth,
  filtersBySpecialistsController.updateFiltersBySpecialist
);
router.post(
  '/get',
  isAuth,
  filtersBySpecialistsController.getFiltersBySpecialist
);
router.get('/get-all', filtersBySpecialistsController.getFiltersBySpecialists);
router.get(
  '/consulte',
  filtersBySpecialistsController.consulteFiltersBySpecialists
);

module.exports = router;
