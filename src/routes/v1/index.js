const utilsRouter = require('./utils-router');
const sendEmailRouter = require('./sendEmail-router');
const usersRouter = require('./users-router');
const customersUserRouter = require('./customersUser-router');
const publicationsRouter = require('./publications-router');
const typePlanRouter = require('./type-plan-router');
const planRouter = require('./plan-router');
const bannersRouter = require('./banners-router');
const commentRouter = require('./comment-router');
const newsRouter = require('./news-router');
const commentNewsRouter = require('./commentNews-router');
const categoriesRouter = require('./categories-router');
const subCategoriesRouter = require('./subCategories-router');
const countriesRouter = require('./countries-router');
const zoneRouter = require('./zone-router');
const districtsRouter = require('./districts-router');
const provincesRouter = require('./province-router');
const promotionalTagsRouter = require('./promotionaltag-router');
const statusesRouter = require('./statuses-router');
const filtersByAgeRouter = require('./filtersByAge-router');
const filtersByCalendarRouter = require('./filtersByCalendares-router');
const filtersByCostesRouter = require('./filtersByCostes-router');
const filtersByLanguageRouter = require('./filtersByLanguage-router');
const filtersByTimeOfDayRouter = require('./filtersByTimeOfDay-router');
const filtersByCenFacilityRouter = require('./filtersByCenFacility-router');
const filtersByLocationsRouter = require('./filtersByLocations-router');
const filtersByCourseTopicsRouter = require('./filtersByCourseTopics-router');
const filtersBySportRouter = require('./filtersBySport-router');
const filtersBySpecialistRouter = require('./filtersBySpecialist-router');
const filtersByServiceRouter = require('./filtersByService-router');
const { upload } = require('../../middlewares/uploadfile');
const { uploadfile } = require('./uploadfile-router');
const imagePostRouter = require('./imagePost-router');

module.exports = (app) => {
  app.use('/api/v1/utils', utilsRouter);
  app.use('/api/v1/sendEmail', sendEmailRouter);
  app.use('/api/v1/users', usersRouter);
  app.use('/api/v1/customersUsers', customersUserRouter);
  app.use('/api/v1/publications', publicationsRouter);
  app.use('/api/v1/type-plan', typePlanRouter);
  app.use('/api/v1/plan', planRouter);
  app.use('/api/v1/banners', bannersRouter);
  app.use('/api/v1/comments', commentRouter);
  app.use('/api/v1/news', newsRouter);
  app.use('/api/v1/comment-news', commentNewsRouter);
  app.use('/api/v1/categories', categoriesRouter);
  app.use('/api/v1/sub-categories', subCategoriesRouter);
  app.use('/api/v1/countries', countriesRouter);
  app.use('/api/v1/zone', zoneRouter);
  app.use('/api/v1/districts', districtsRouter);
  app.use('/api/v1/provinces', provincesRouter);
  app.use('/api/v1/promotional-tags', promotionalTagsRouter);
  app.use('/api/v1/statuses', statusesRouter);
  app.use('/api/v1/filtersByAge', filtersByAgeRouter);
  app.use('/api/v1/filtersByCalendar', filtersByCalendarRouter);
  app.use('/api/v1/filtersByCostes', filtersByCostesRouter);
  app.use('/api/v1/filtersByLanguage', filtersByLanguageRouter);
  app.use('/api/v1/filtersByTimeOfDay', filtersByTimeOfDayRouter);
  app.use('/api/v1/filtersByCenFacility', filtersByCenFacilityRouter);
  app.use('/api/v1/filtersByLocation', filtersByLocationsRouter);
  app.use('/api/v1/filtersByCourseTopics', filtersByCourseTopicsRouter);
  app.use('/api/v1/filtersBySport', filtersBySportRouter);
  app.use('/api/v1/filtersBySpecialist', filtersBySpecialistRouter);
  app.use('/api/v1/filtersByService', filtersByServiceRouter);
  app.use('/api/v1/image-upload', upload.single('file'), uploadfile);
  app.use('/api/v1/imagePost', imagePostRouter);
};
