const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const bannersController = require('../../controllers/v1/banners-controller');

const router = express.Router();

router.post('/create', isAuth, bannersController.createBanner);
router.post('/delete', isAuth, bannersController.deleteBanner);
router.post('/update', isAuth, bannersController.updateBanner);
router.post('/get', isAuth, bannersController.getBanner);
router.get('/get-all', bannersController.getBanners);
router.get('/get-otro-all', bannersController.getOtroBanners);
router.get('/get-all-filter', bannersController.getBannersFilters);
router.get('/get-banners-all', bannersController.getBannersAll);
router.post('/counted-banner-view', bannersController.countedBannerView);

module.exports = router;
