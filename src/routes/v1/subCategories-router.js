const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const subCategoriesController = require('../../controllers/v1/subCategories-controller');

const router = express.Router();

router.post('/create', isAuth, subCategoriesController.createSubCategorie);
router.post('/delete', isAuth, subCategoriesController.deleteSubCategorie);
router.post('/update', isAuth, subCategoriesController.updateSubCategorie);
router.post('/get', subCategoriesController.getSubCategorie);
router.get('/get-all', subCategoriesController.getSubCategories);
router.get('/get-all-post', subCategoriesController.getSubCategoriesAndPost);
router.get('/consultation', subCategoriesController.consulteSubCategories);
router.post(
  '/consultation-for-categories',
  subCategoriesController.consulteSubCategoriesFromCategories
);

module.exports = router;
