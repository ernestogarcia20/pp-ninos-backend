const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const categoriesController = require('../../controllers/v1/categories-controller');

const router = express.Router();

router.post('/create', isAuth, categoriesController.createCategorie);
router.post('/delete', isAuth, categoriesController.deleteCategorie);
router.post('/update', isAuth, categoriesController.updateCategorie);
router.post('/get', categoriesController.getCategorie);
router.get('/get-all', categoriesController.getCategories);
router.get('/get-all-post', categoriesController.getCategoriesAndPost);
router.get('/consultation', categoriesController.consulteCategories);

module.exports = router;
