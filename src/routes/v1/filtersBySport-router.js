const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersBySportsController = require('../../controllers/v1/filtersBySport-controller');

const router = express.Router();

router.post('/create', isAuth, filtersBySportsController.createFiltersBySport);
router.post('/delete', isAuth, filtersBySportsController.deleteFiltersBySport);
router.post('/update', isAuth, filtersBySportsController.updateFiltersBySport);
router.post('/get', isAuth, filtersBySportsController.getFiltersBySport);
router.get('/get-all', filtersBySportsController.getFiltersBySports);
router.get('/consulte', filtersBySportsController.consulteFiltersBySports);

module.exports = router;
