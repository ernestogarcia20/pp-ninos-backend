const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersByCalendaresController = require('../../controllers/v1/filtersByCalendares-controller');

const router = express.Router();

router.post(
  '/create',
  isAuth,
  filtersByCalendaresController.createFiltersByCalendar
);
router.post(
  '/delete',
  isAuth,
  filtersByCalendaresController.deleteFiltersByCalendar
);
router.post(
  '/update',
  isAuth,
  filtersByCalendaresController.updateFiltersByCalendar
);
router.post('/get', isAuth, filtersByCalendaresController.getFiltersByCalendar);
router.get('/get-all', filtersByCalendaresController.getFiltersByCalendares);
router.get(
  '/consulte',
  filtersByCalendaresController.consulteFiltersByCalendares
);

module.exports = router;
