const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersByCourseTopicsController = require('../../controllers/v1/filtersByCourseTopics-controller');

const router = express.Router();

router.post(
  '/create',
  isAuth,
  filtersByCourseTopicsController.createFiltersByCourseTopic
);
router.post(
  '/delete',
  isAuth,
  filtersByCourseTopicsController.deleteFiltersByCourseTopic
);
router.post(
  '/update',
  isAuth,
  filtersByCourseTopicsController.updateFiltersByCourseTopic
);
router.post(
  '/get',
  isAuth,
  filtersByCourseTopicsController.getFiltersByCourseTopic
);
router.get(
  '/get-all',
  filtersByCourseTopicsController.getFiltersByCourseTopics
);
router.get(
  '/consulte',
  filtersByCourseTopicsController.consulteFiltersByCourseTopics
);

module.exports = router;
