const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersByCostesController = require('../../controllers/v1/filtersByCostes-controller');

const router = express.Router();

router.post('/create', isAuth, filtersByCostesController.createFiltersByCoste);
router.post('/delete', isAuth, filtersByCostesController.deleteFiltersByCoste);
router.post('/update', isAuth, filtersByCostesController.updateFiltersByCoste);
router.post('/get', isAuth, filtersByCostesController.getFiltersByCoste);
router.get('/get-all', filtersByCostesController.getFiltersByCostes);
router.get('/consulte', filtersByCostesController.consulteFiltersByCostes);

module.exports = router;
