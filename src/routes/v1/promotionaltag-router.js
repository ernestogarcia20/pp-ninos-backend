const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const promotionalTagsController = require('../../controllers/v1/promotionaltag-controller');

const router = express.Router();

router.post('/create', isAuth, promotionalTagsController.createPromotionalTag);
router.post('/delete', isAuth, promotionalTagsController.deletePromotionalTag);
router.post('/update', isAuth, promotionalTagsController.updatePromotionalTag);
router.post('/get', isAuth, promotionalTagsController.getPromotionalTag);
router.post('/get-for-name', promotionalTagsController.getTagForName);
router.get('/get-all', promotionalTagsController.getPromotionalTags);
router.get('/consulte', promotionalTagsController.consultePromotionalTags);

module.exports = router;
