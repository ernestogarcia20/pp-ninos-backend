const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersByServicesController = require('../../controllers/v1/filtersByService-controller');

const router = express.Router();

router.post(
  '/create',
  isAuth,
  filtersByServicesController.createFiltersByService
);
router.post(
  '/delete',
  isAuth,
  filtersByServicesController.deleteFiltersByService
);
router.post(
  '/update',
  isAuth,
  filtersByServicesController.updateFiltersByService
);
router.post('/get', isAuth, filtersByServicesController.getFiltersByService);
router.get('/get-all', filtersByServicesController.getFiltersByServices);
router.get('/consulte', filtersByServicesController.consulteFiltersByServices);

module.exports = router;
