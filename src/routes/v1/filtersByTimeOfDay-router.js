const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersByTimeOfDaysController = require('../../controllers/v1/filtersByTimeOfDay-controller');

const router = express.Router();

router.post(
  '/create',
  isAuth,
  filtersByTimeOfDaysController.createFiltersByTimeOfDay
);
router.post(
  '/delete',
  isAuth,
  filtersByTimeOfDaysController.deleteFiltersByTimeOfDay
);
router.post(
  '/update',
  isAuth,
  filtersByTimeOfDaysController.updateFiltersByTimeOfDay
);
router.post(
  '/get',
  isAuth,
  filtersByTimeOfDaysController.getFiltersByTimeOfDay
);
router.get('/get-all', filtersByTimeOfDaysController.getFiltersByTimeOfDays);
router.get(
  '/consulte',
  filtersByTimeOfDaysController.consulteFiltersByTimeOfDays
);

module.exports = router;
