const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersByLanguagesController = require('../../controllers/v1/filtersByLanguage-controller');

const router = express.Router();

router.post(
  '/create',
  isAuth,
  filtersByLanguagesController.createFiltersByLanguage
);
router.post(
  '/delete',
  isAuth,
  filtersByLanguagesController.deleteFiltersByLanguage
);
router.post(
  '/update',
  isAuth,
  filtersByLanguagesController.updateFiltersByLanguage
);
router.post('/get', isAuth, filtersByLanguagesController.getFiltersByLanguage);
router.get('/get-all', filtersByLanguagesController.getFiltersByLanguages);
router.get(
  '/consulte',
  filtersByLanguagesController.consulteFiltersByLanguages
);

module.exports = router;
