const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const newsController = require('../../controllers/v1/news-controller');

const router = express.Router();

router.post('/create', isAuth, newsController.createNews);
router.post('/delete', isAuth, newsController.deleteNews);
router.post('/edit', isAuth, newsController.editNews);
router.post('/get', newsController.getNew);
router.post('/get-name', newsController.getNewForName);
router.post('/get-consult-id', newsController.getNewConsult);
router.get('/get-all', newsController.getNews);
router.get('/get-all-pages', newsController.getPageNews);
router.get('/get-consults', newsController.getCosultNews);

module.exports = router;
