const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const statusesController = require('../../controllers/v1/statuses-controller');

const router = express.Router();

router.post('/create', isAuth, statusesController.createStatus);
router.post('/delete', isAuth, statusesController.deleteStatus);
router.post('/update', isAuth, statusesController.updateStatus);
router.post('/get', isAuth, statusesController.getStatus);
router.get('/get-all', statusesController.getStatuses);
router.get('/consulte', statusesController.consulteStatuses);

module.exports = router;
