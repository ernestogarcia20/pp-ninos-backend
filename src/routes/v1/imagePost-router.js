const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const imagePostController = require('../../controllers/v1/imagePost-controller');

const router = express.Router();

router.post('/create', isAuth, imagePostController.createImagePost);
router.post('/delete', isAuth, imagePostController.deleteImagePost);
router.post('/deleteImages', isAuth, imagePostController.deleteImagesPosts);
router.post('/update', isAuth, imagePostController.updateImagePost);
router.post('/get', imagePostController.getImagePost);

module.exports = router;
