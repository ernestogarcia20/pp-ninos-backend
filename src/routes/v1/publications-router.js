const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const publicationsController = require('../../controllers/v1/publications-controller');

const router = express.Router();

router.post('/create', isAuth, publicationsController.createPublications);
router.post('/delete', isAuth, publicationsController.deletePublications);
router.post('/update', isAuth, publicationsController.editPublications);
router.post('/get', publicationsController.getPublication);
router.post('/get-name', publicationsController.getPublicationForName);
router.post('/get-branch', publicationsController.getBranchPublications);
router.post('/get-similar', publicationsController.similarPosts);
router.post(
  '/multiConsulte',
  isAuth,
  publicationsController.multiConsultePublication
);
router.post(
  '/get-all-customer',
  isAuth,
  publicationsController.getPublicationsForCustomer
);
router.get('/get-all', publicationsController.getPublications);
router.get(
  '/get-all-promoTag',
  publicationsController.getPublicationsAndPromotionalTag
);
router.get('/get-all-branch', publicationsController.getPublicationsBranch);
router.get('/multi-get-all', publicationsController.multiGetPublications);
router.get(
  '/multi-get-all-filter',
  publicationsController.multiGetPublicationsFilter
);
router.get('/get-pages/', publicationsController.getPagePublications);
router.get('/get-filter/', publicationsController.getFilterPublications);
router.post('/search', publicationsController.searchPublication);
router.post('/searchPost', isAuth, publicationsController.searchPost);

module.exports = router;
