const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const commentController = require('../../controllers/v1/comment-controller');

const router = express.Router();

router.post('/create', commentController.createComment);
router.post('/delete', isAuth, commentController.deleteComment);
router.post('/update', isAuth, commentController.updateComment);
router.post('/get', commentController.getComment);
router.post('/get-all', commentController.getComments);

module.exports = router;
