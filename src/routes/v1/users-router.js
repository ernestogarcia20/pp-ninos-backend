const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const usersController = require('../../controllers/v1/users-controller');

const router = express.Router();

router.post('/create', usersController.createUser);
router.post('/delete', isAuth, usersController.deleteUser);
router.post('/update', isAuth, usersController.updateUser);
router.get('/get', isAuth, usersController.getUser);
router.get('/get-all', usersController.getUsers);
router.get('/consulte', usersController.consulteUsers);
router.post('/login', usersController.loginUser);

module.exports = router;
