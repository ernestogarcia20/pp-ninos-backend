const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const zoneController = require('../../controllers/v1/zone-controller');

const router = express.Router();

router.post('/create', isAuth, zoneController.createZone);
router.post('/delete', isAuth, zoneController.deleteZone);
router.post('/update', isAuth, zoneController.updateZone);
router.post('/get', isAuth, zoneController.getZone);
router.post('/get-consulte', isAuth, zoneController.consulteZone);
router.post(
  '/get-consultes-for-district',
  zoneController.consulteZonesForDistrict
);
router.get('/get-all', zoneController.getZones);
router.get('/consulte', zoneController.consulteZones);

module.exports = router;
