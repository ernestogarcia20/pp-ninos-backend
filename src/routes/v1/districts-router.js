const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const districtsController = require('../../controllers/v1/districts-controller');

const router = express.Router();

router.post('/create', isAuth, districtsController.createDistrict);
router.post('/delete', isAuth, districtsController.deleteDistrict);
router.post('/update', isAuth, districtsController.updateDistrict);
router.post('/get', isAuth, districtsController.getDistrict);
router.post('/get-consulte', isAuth, districtsController.consulteDistrict);
router.post(
  '/get-consultes-for-province',
  districtsController.consulteDistrictsForProvince
);
router.get('/get-all', districtsController.getDistricts);
router.get('/consultes', districtsController.consulteDistricts);

module.exports = router;
