const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersByAgesController = require('../../controllers/v1/filtersByAge-controller');

const router = express.Router();

router.post('/create', isAuth, filtersByAgesController.createFiltersByAge);
router.post('/delete', isAuth, filtersByAgesController.deleteFiltersByAge);
router.post('/update', isAuth, filtersByAgesController.updateFiltersByAge);
router.post('/get', isAuth, filtersByAgesController.getFiltersByAge);
router.get('/get-all', filtersByAgesController.getFiltersByAges);
router.get('/consulte', filtersByAgesController.consulteFiltersByAges);

module.exports = router;
