const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const filtersByLocationsController = require('../../controllers/v1/filtersByLocations-controller');

const router = express.Router();

router.post(
  '/create',
  isAuth,
  filtersByLocationsController.createFiltersByLocation
);
router.post(
  '/delete',
  isAuth,
  filtersByLocationsController.deleteFiltersByLocation
);
router.post(
  '/update',
  isAuth,
  filtersByLocationsController.updateFiltersByLocation
);
router.post('/get', isAuth, filtersByLocationsController.getFiltersByLocation);
router.get('/get-all', filtersByLocationsController.getFiltersByLocations);
router.get(
  '/consulte',
  filtersByLocationsController.consulteFiltersByLocations
);

module.exports = router;
