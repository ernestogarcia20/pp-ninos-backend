const uploadfile = (req, res) => {
  res.send({
    status: 'Ok',
    message: req.file,
  });
};

module.exports = { uploadfile };
