const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const typePlanController = require('../../controllers/v1/type-plan-controller');

const router = express.Router();

router.post('/create', isAuth, typePlanController.createTypePlan);
router.post('/delete', isAuth, typePlanController.deleteTypePlan);
router.post('/update', isAuth, typePlanController.updateTypePlan);
router.get('/get', isAuth, typePlanController.getTypePlan);
router.get('/get-all', isAuth, typePlanController.getTypesPlan);

module.exports = router;
