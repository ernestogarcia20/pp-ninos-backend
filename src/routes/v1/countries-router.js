const express = require('express');
const { isAuth } = require('../../middlewares/auth');
const countriesController = require('../../controllers/v1/countries-controller');

const router = express.Router();

router.post('/create', isAuth, countriesController.createCountry);
router.post('/delete', isAuth, countriesController.deleteCountry);
router.post('/update', isAuth, countriesController.updateCountry);
router.post('/get', isAuth, countriesController.getCountry);
router.get('/get-all', countriesController.getCountries);
router.get('/consultes', countriesController.consulteCountries);

module.exports = router;
