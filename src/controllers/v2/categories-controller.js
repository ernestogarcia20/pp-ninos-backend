const conexion = require('../../sql/conexion');

const getCategorieForName = (req, res) => {
  const { nameCategory } = req.body;
  const sql = `SELECT * FROM categories WHERE name = ${nameCategory}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

module.exports = {
  getCategorieForName,
};
