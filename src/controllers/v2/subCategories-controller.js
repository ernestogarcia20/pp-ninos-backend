const conexion = require('../../sql/conexion');

const consulteSubCategoriesFromCategories = (req, res) => {
  const { idCategories } = req.body;
  const sql = `SELECT * FROM subCategory WHERE id_category IN (${idCategories})`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

module.exports = { consulteSubCategoriesFromCategories };
