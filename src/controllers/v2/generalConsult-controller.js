const conexion = require('../../sql/conexion');
const geoip = require('geoip-lite');
const iplocate = require('node-iplocate');
const axios = require('axios');

const getConsultSeletOption = (req, res) => {
  const { nameTable, value, label } = req.body.table;
  const sql = `SELECT ${value} AS 'value', ${label} AS 'label' FROM ${nameTable}`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getMultiTableConsulte = (req, res) => {
  const { id } = req.body.data;
  const { namesTables, principalTable, filters } = req.body.tables;
  const sql = `SELECT * FROM ${namesTables} WHERE ${principalTable}.idCustomer = '${id}' AND ${filters}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const createData = (req, res) => {
  const { nameTable } = req.body.table;
  const sql = `INSERT INTO ${nameTable} SET ?`;
  const userObj = req.body.info;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Publications Create!' });
  });
};

const editData = (req, res) => {
  const { id, where } = req.body.data;
  const userObj = req.body.info;
  const { nameTable } = req.body.table;
  const sql = `UPDATE ${nameTable} SET ? WHERE ${where} = '${id}'`;

  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Publications updated' });
  });
};

const updata = (req, res) => {
  const { id, where } = req.body.data;
  const { column, data } = req.body.info;
  const { nameTable } = req.body.table;
  const sql = `UPDATE ${nameTable} SET ${column}='${data}' WHERE ${where} = '${id}'`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Publications updated' });
  });
};

const getData = (req, res) => {
  const { id, where } = req.body.data;
  const { nameTable } = req.body.table;
  const sql = `SELECT * FROM ${nameTable} WHERE ${where} = '${id}'`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getAllData = (req, res) => {
  const { nameTable } = req.body.table;
  const sql = `SELECT * FROM ${nameTable}`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const deleteData = (req, res) => {
  const { id, where } = req.body.data;
  const { nameTable } = req.body.table;
  const sql = `DELETE FROM ${nameTable} WHERE ${where} = '${id}'`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Data deleted' });
  });
};

const clicksComponent = (req, res) => {
  const { nameTable, data } = req.body;
  const sql = `INSERT INTO ${nameTable} SET ?`;
  conexion.query(sql, data, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Save click!' });
  });
};

const clicksPublications = async (req, res) => {
  const { nameTable, data } = req.body;
  const { ExternalIP } = data;
  const datosISP = await datosIP(ExternalIP);
  const dataGeo = {
    isp: datosISP,
    countryConnection: geoip.lookup(ExternalIP).country,
    cityConexion: geoip.lookup(ExternalIP).city,
  };
  Object.assign(data, dataGeo);
  const sql = `INSERT INTO ${nameTable} SET ?`;
  conexion.query(sql, data, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Save click!' });
  });
};

const generalGetClicksPublications = (req, res) => {
  const { nameTable } = req.body;
  conexion.query(
    [
      `SELECT * FROM ${nameTable} WHERE isp != 'Google LLC' ORDER BY dataClick ASC LIMIT 1`,
      `SELECT * FROM ${nameTable} WHERE isp != 'Google LLC' ORDER BY dataClick DESC LIMIT 1`,
      `SELECT COUNT(pointAccess) AS totalClicks FROM ${nameTable} WHERE isp != 'Google LLC' AND pointAccess = 'ConteoGlobal'`,
      `SELECT COUNT(pointAccess) AS totalClicksValidos FROM ${nameTable} WHERE isp != 'Google LLC' AND pointAccess = 'ConteoGlobal' AND error_idPost=0`,
      `SELECT COUNT(pointAccess) AS totalClicksError FROM ${nameTable} WHERE isp != 'Google LLC' AND pointAccess = 'ConteoGlobal' AND error_idPost=1`,
      `SELECT COUNT(pointAccess) AS totalClicksEspecificos FROM ${nameTable} WHERE isp != 'Google LLC' AND pointAccess != 'ConteoGlobal' AND error_idPost=0`,
      `SELECT pointAccess,COUNT(pointAccess) AS clicksForPages FROM ${nameTable} WHERE isp != 'Google LLC' AND pointAccess != 'ConteoGlobal' AND error_idPost=0 GROUP BY pointAccess`,
      `SELECT COUNT(isMobile) AS totalClicksIsMobile FROM ${nameTable} WHERE isp != 'Google LLC' AND isMobile = 1 AND pointAccess = 'ConteoGlobal' AND error_idPost=0`,
      `SELECT COUNT(isMobile) AS totalCliscksNotIsMobile FROM ${nameTable} WHERE isp != 'Google LLC' AND isMobile = 0 AND pointAccess = 'ConteoGlobal' AND error_idPost=0`,
      `SELECT countryConnection,COUNT(countryConnection) AS totalClicksForCoutry FROM ${nameTable} WHERE isp != 'Google LLC' AND pointAccess = 'ConteoGlobal' AND error_idPost=0 GROUP BY countryConnection`,
    ].join(';'),
    (error, rows, fields) => {
      if (error) throw error;
      res.send({
        status: 'OK',
        data: {
          initDate: rows[0][0].dataClick,
          finalDate: rows[1][0].dataClick,
          totalClicks: rows[2][0].totalClicks,
          clicksValidos: rows[3][0].totalClicksValidos,
          clicksError: rows[4][0].totalClicksError,
          clicksInternal: rows[5][0].totalClicksEspecificos,
          clicksForDevice: {
            movile: rows[7][0].totalClicksIsMobile,
            PC: rows[8][0].totalCliscksNotIsMobile,
          },
          clicksInternalForPoint: rows[6],
          clicksForCountry: rows[9],
        },
      });
    }
  );
};

const getClicksPerPost = (req, res) => {
  const { nameTable, idPost } = req.body;
  conexion.query(
    [
      `SELECT * FROM ${nameTable} WHERE isp != 'Google LLC' AND idPost = '${idPost}' ORDER BY dataClick ASC LIMIT 1`,
      `SELECT * FROM ${nameTable} WHERE isp != 'Google LLC' AND idPost = '${idPost}' ORDER BY dataClick DESC LIMIT 1`,
      `SELECT COUNT(pointAccess) AS totalClicks FROM ${nameTable} WHERE isp != 'Google LLC' AND idPost = '${idPost}' AND pointAccess = 'ConteoGlobal'`,
      `SELECT COUNT(pointAccess) AS totalClicksValidos FROM ${nameTable} WHERE isp != 'Google LLC' AND idPost = '${idPost}' AND pointAccess = 'ConteoGlobal' AND error_idPost=0`,
      `SELECT COUNT(pointAccess) AS totalClicksError FROM ${nameTable} WHERE isp != 'Google LLC' AND idPost = '${idPost}' AND pointAccess = 'ConteoGlobal' AND error_idPost=1`,
      `SELECT COUNT(pointAccess) AS totalClicksEspecificos FROM ${nameTable} WHERE isp != 'Google LLC' AND idPost = '${idPost}' AND pointAccess != 'ConteoGlobal' AND error_idPost=0`,
      `SELECT pointAccess,COUNT(pointAccess) AS clicksForPages FROM ${nameTable} WHERE isp != 'Google LLC' AND idPost = '${idPost}' AND pointAccess != 'ConteoGlobal' AND error_idPost=0 GROUP BY pointAccess`,
      `SELECT COUNT(isMobile) AS totalClicksIsMobile FROM ${nameTable} WHERE isp != 'Google LLC' AND idPost = '${idPost}' AND isMobile = 1 AND pointAccess = 'ConteoGlobal' AND error_idPost=0`,
      `SELECT COUNT(isMobile) AS totalCliscksNotIsMobile FROM ${nameTable} WHERE isp != 'Google LLC' AND idPost = '${idPost}' AND isMobile = 0 AND pointAccess = 'ConteoGlobal' AND error_idPost=0`,
      `SELECT countryConnection,COUNT(countryConnection) AS totalClicksForCoutry FROM ${nameTable} WHERE isp != 'Google LLC' AND idPost = '${idPost}' AND pointAccess = 'ConteoGlobal' AND error_idPost=0 GROUP BY countryConnection`,
    ].join(';'),
    (error, rows, fields) => {
      if (error) throw error;
      if (rows[2][0].totalClicks > 0) {
        res.send({
          status: 'OK',
          data: {
            initDate: rows[0][0].dataClick,
            finalDate: rows[1][0].dataClick,
            totalClicks: rows[2][0].totalClicks,
            clicksValidos: rows[3][0].totalClicksValidos,
            clicksError: rows[4][0].totalClicksError,
            clicksInternal: rows[5][0].totalClicksEspecificos,
            clicksForDevice: {
              movile: rows[7][0].totalClicksIsMobile,
              PC: rows[8][0].totalCliscksNotIsMobile,
            },
            clicksInternalForPoint: rows[6],
            clicksForCountry: rows[9],
          },
        });
      } else {
        res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
      }
    }
  );
};

const datosIP = async (ip) => {
  let value = '';
  const res = await axios.get(`http://ip-api.com/json/${ip}`);
  if (res.data.isp) {
    value = res.data.isp;
    console.log(res.data.isp);
  } else {
    value = 'Desconocido';
  }
  return value;
};

const ispAsigna = async (req, res) => {
  conexion.query(
    "SELECT * FROM clicksPost WHERE isp='Desconocido'",
    async (error, result) => {
      if (error) throw error;
      if (result.length > 0) {
        for (let i = 0; i < result.length; i++) {
          const isp = await datosIP(result[i].ExternalIP);
          conexion.query(
            `UPDATE clicksPost SET isp = '${isp}' WHERE idClickPost = ${result[i].idClickPost}`,
            (e) => {
              if (e) throw e;
              console.log(
                `Se actualizo el ISP a ${isp} para la IP ${result[i].ExternalIP}`
              );
            }
          );
        }
        res.send({ message: 'Proceso finalizado' }).end();
      }
    }
  );
};

module.exports = {
  getConsultSeletOption,
  getMultiTableConsulte,
  editData,
  createData,
  getData,
  getAllData,
  deleteData,
  updata,
  clicksPublications,
  generalGetClicksPublications,
  getClicksPerPost,
  clicksComponent,
  ispAsigna,
};
