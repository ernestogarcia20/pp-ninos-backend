const conexion = require('../../sql/conexion');

const createBanner = async (req, res) => {
  const userObj = req.body;
  const sql = 'INSERT INTO sideBanner SET ?';
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send('Banner Create!');
  });
};

const deleteBanner = (req, res) => {
  const { idbanner } = req.body;
  const sql = `DELETE FROM sideBanner WHERE idbanner = '${idbanner}'`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Banner deleted' });
  });
};

const getBanners = (req, res) => {
  const sql = `SELECT * FROM sideBanner ORDER BY Position LIMIT ${req.query.limit}`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getOtroBanners = (req, res) => {
  const sql = 'SELECT * FROM sliderBanner';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getBannersAll = (req, res) => {
  const sql = 'SELECT * FROM sideBanner ORDER BY Position';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getBannersFilters = (req, res) => {
  const { ubicacion, data, table } = req.query;
  const currentDate = new Date().getTime();
  const sql = `SELECT * FROM sideBanner WHERE status = 'activated' AND disablingDate > ${currentDate} AND ubicacion = ${ubicacion} AND ${table} LIKE '%${data}%' ORDER BY Position`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getBanner = (req, res) => {
  const { idbanner } = req.body;
  const sql = `SELECT * FROM sideBanner WHERE idbanner = '${idbanner}'`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateBanner = (req, res) => {
  const { idbanner } = req.body;
  const bannerObj = req.body;

  const sql = `UPDATE sideBanner SET ? WHERE idbanner = '${idbanner}'`;

  conexion.query(sql, bannerObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Benner updated' });
  });
};

const countedBannerView = async (req, res) => {
  const userObj = req.body;
  const sql = 'INSERT INTO bannersView SET ?';
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Benner view counted' });
  });
};

module.exports = {
  createBanner,
  deleteBanner,
  getBanner,
  getBanners,
  updateBanner,
  getBannersFilters,
  getBannersAll,
  countedBannerView,
  getOtroBanners,
};
