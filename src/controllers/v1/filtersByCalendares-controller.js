const conexion = require('../../sql/conexion');

const createFiltersByCalendar = async (req, res) => {
  const sql = 'INSERT INTO filtersByCalendar SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Calendar Create!' });
  });
};

const deleteFiltersByCalendar = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersByCalendar WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Calendar Deleted' });
  });
};

const getFiltersByCalendares = (req, res) => {
  const sql = 'SELECT * FROM filtersByCalendar';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersByCalendares = (req, res) => {
  const sql =
    'SELECT keyword AS `value`, name AS `label` FROM filtersByCalendar';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersByCalendar = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersByCalendar WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersByCalendar = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersByCalendar SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Calendar updated' });
  });
};

module.exports = {
  createFiltersByCalendar,
  deleteFiltersByCalendar,
  getFiltersByCalendar,
  getFiltersByCalendares,
  consulteFiltersByCalendares,
  updateFiltersByCalendar,
};
