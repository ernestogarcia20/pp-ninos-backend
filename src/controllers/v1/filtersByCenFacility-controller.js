const conexion = require('../../sql/conexion');

const createFiltersByCenFacility = async (req, res) => {
  const sql = 'INSERT INTO filtersByCenFacility SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Age Create!' });
  });
};

const deleteFiltersByCenFacility = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersByCenFacility WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Age Deleted' });
  });
};

const getFiltersByCenFacilities = (req, res) => {
  const sql = 'SELECT * FROM filtersByCenFacility';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersByCenFacilities = (req, res) => {
  const sql =
    'SELECT keyword AS `value`, name AS `label` FROM filtersByCenFacility';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersByCenFacility = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersByCenFacility WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersByCenFacility = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersByCenFacility SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Age updated' });
  });
};

module.exports = {
  createFiltersByCenFacility,
  deleteFiltersByCenFacility,
  getFiltersByCenFacility,
  getFiltersByCenFacilities,
  consulteFiltersByCenFacilities,
  updateFiltersByCenFacility,
};
