const conexion = require('../../sql/conexion');

const createDistrict = async (req, res) => {
  const sql = 'INSERT INTO city SET ?';
  const userObj = {
    name: req.body.name,
    province: req.body.province,
  };
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'City Create!' });
  });
};

const deleteDistrict = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM city WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'City Deleted' });
  });
};

const getDistricts = (req, res) => {
  const sql = 'SELECT * FROM city ORDER BY name';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteDistricts = (req, res) => {
  const sql = 'SELECT name AS `value`, name AS `label` FROM city ORDER BY name';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteDistrictsForProvince = (req, res) => {
  const { province } = req.body;
  const sql = `SELECT name AS 'value', name AS 'label' FROM city WHERE province = '${province}' ORDER BY name`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getDistrict = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM city WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateDistrict = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const province = `province='${req.body.province}'`;

  const queryVariables = `${name}${province}`;

  const sql = `UPDATE city SET ${queryVariables} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'City updated' });
  });
};

const consulteDistrict = (req, res) => {
  const { name } = req.body;
  const sql = `SELECT province.name AS 'province', countries.name AS 'country' FROM city, province, countries WHERE city.name='${name}' AND city.province=province.name AND province.country=countries.name ORDER BY name`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

module.exports = {
  createDistrict,
  deleteDistrict,
  getDistrict,
  getDistricts,
  consulteDistricts,
  updateDistrict,
  consulteDistrict,
  consulteDistrictsForProvince,
};
