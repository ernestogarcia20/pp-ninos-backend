const conexion = require('../../sql/conexion');

const createImagePost = async (req, res) => {
  const userObj = req.body;
  const sql = 'INSERT INTO imagePost SET ?';
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Image Post create' });
  });
};

const deleteImagePost = (req, res) => {
  const { idImage } = req.body;
  const sql = `DELETE FROM imagePost WHERE idImage = '${idImage}'`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Image Post deleted' });
  });
};

const deleteImagesPosts = (req, res) => {
  const { idPost } = req.body;
  const sql = `DELETE FROM imagePost WHERE idPost = '${idPost}'`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Images Posts deleted' });
  });
};

const getImagePost = (req, res) => {
  const { idPost } = req.body;
  const sql = `SELECT * FROM imagePost WHERE idPost = '${idPost}'`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateImagePost = (req, res) => {
  const { idbanner } = req.body;
  const bannerObj = req.body;

  const sql = `UPDATE sideBanner SET ? WHERE idbanner = '${idbanner}'`;

  conexion.query(sql, bannerObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Benner updated' });
  });
};

module.exports = {
  createImagePost,
  deleteImagePost,
  deleteImagesPosts,
  updateImagePost,
  getImagePost,
};
