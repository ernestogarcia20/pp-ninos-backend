const nodemailer = require('nodemailer');

const sendEmailCentroAyuda = (req, res) => {
  const htmlEmail = `<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Email</title>
      </head>
      <body>
        <div style="width: 100%; height: 100%; background-color: white;">
          <div>
            <div>
              <img src="https://${process.env.NEXT_PUBLIC_HOSTNAME_WEB}/images/headerEmail.jpg" alt="Header Email" />
            </div>
          </div>
          <div>
            <div style="color: #a2c835;"><h1>Bienvenid@ a Panamá para niños</h1></div>
            <div>
              <p>
                Hola, les contacto a través del centro de ayuda de Panamá para niños
                para la siguiente consulta:
              </p>
              <p>${req.body.text}</p>
              <p>Mis datos de contacto son:</p>
              <p><b>Nombre:</b> ${req.body.name}</p>
              <p><b>E-mail:</b> ${req.body.email}</p>
              <p>Por ser un correo masivo, le agradecemos no conteste a esta dirección.</p>
            </div>
          </div>
        </div>
      </body>
    </html>`;
  const transporter = nodemailer.createTransport({
    host: 'mail.panamaparaninos.com',
    port: 465,
    secure: true,
    auth: {
      user: 'centro.ayuda@panamaparaninos.com',
      pass: 'Panama2021.',
    },
  });
  const mailOptions = {
    from: 'centro.ayuda@panamaparaninos.com',
    to: 'info@panamaparaninos.com',
    bcc: 'panamaparaninos@gmail.com',
    subject: `Correo de ${req.body.name}`,
    text: req.body.text,
    html: htmlEmail,
  };

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      res.status(500).send(err.message);
      console.log(err.message);
    } else {
      console.log('Mensaje enviado', info);
      res.status(200).json(req.body);
    }
  });

  const htmlEmailClient = `<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Email</title>
      </head>
      <body>
        <div style="width: 100%; height: 100%; background-color: white;">
          <div>
            <div>
              <img src="https://${process.env.NEXT_PUBLIC_HOSTNAME_WEB}/images/headerEmail.jpg" alt="Header Email" />
            </div>
          </div>
          <div>
            <div style="color: #a2c835;"><h1>Bienvenid@ a Panamá para niños</h1></div>
            <div>
              <p>
                Hola, se ha contactado  con nosotros través del centro de ayuda de Panamá para niños
                para la siguiente consulta:
              </p>
              <p>${req.body.text}</p>
              <p>Los datos que nos ha suministrado son:</p>
              <p><b>Nombre:</b> ${req.body.name}</p>
              <p><b>E-mail:</b> ${req.body.email}</p>
              <p>Por ser un correo masivo, le agradecemos no conteste a esta dirección.</p>
            </div>
          </div>
        </div>
      </body>
    </html>`;

  const mailOptionsClient = {
    from: 'centro.ayuda@panamaparaninos.com',
    to: req.body.email,
    subject: `Correo de ${req.body.name}`,
    text: req.body.text,
    html: htmlEmailClient,
  };

  transporter.sendMail(mailOptionsClient, (err, info) => {
    if (err) {
      res.status(500).send(err.message);
      console.log(err.message);
    } else {
      console.log('Mensaje enviado', info);
      res.status(200).json(req.body);
    }
  });
};

const sendEmailComment = (req, res) => {
  const htmlEmail = `<!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Email</title>
    </head>
    <body>
      <div style="width: 100%; height: 100%; background-color: white;">
        <div>
          <div style="color: #a2c835;"><h1>Bienvenid@ a Panamá para niños</h1></div>
          <div>
            <p>
              Hola ${req.body.nameProvider}, les contacto a través del centro de ayuda de Panamá para niños
              para la siguiente consulta:
            </p>
            <p>${req.body.text}</p>
            <p>Mis datos de contacto son:</p>
            <p><b>Nombre:</b> ${req.body.name}</p>
            <p><b>E-mail:</b> ${req.body.email}</p>
            <p><b>Telefono:</b> ${req.body.phone}</p>
            </br>
          </div>
        </div>
      </div>
    </body>
  </html>`;
  const transporter = nodemailer.createTransport({
    host: 'mail.panamaparaninos.com',
    port: 465,
    secure: true,
    auth: {
      user: 'comentarios@panamaparaninos.com',
      pass: 'Panama2021.',
    },
  });
  const mailOptions = {
    from: 'comentarios@panamaparaninos.com',
    to:
      req.body.idPlan === 100300 ||
      req.body.idPlan === 100301 ||
      req.body.idPlan === 100302 ||
      req.body.idPlan === 100400
        ? `${req.body.emailProvider}`
        : 'panamaparaninos@gmail.com',
    bcc: 'info@panamaparaninos.com',
    subject: `Comentario enviado por ${req.body.name}`,
    text: req.body.text,
    html: htmlEmail,
  };

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      res.status(500).send(err.message);
    } else {
      console.log('Mensaje enviado', info);
      res.status(200).json(req.body);
    }
  });
};

module.exports = {
  sendEmailCentroAyuda,
  sendEmailComment,
};
