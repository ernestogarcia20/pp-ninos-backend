const conexion = require('../../sql/conexion');

const createCommentNews = async (req, res) => {
  const sql = 'INSERT INTO commentNews SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Comment Create!' });
  });
};

const getCommentsNews = (req, res) => {
  const { idNews } = req.body;
  const sql = `SELECT * FROM commentNews WHERE idNews = '${idNews}'`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results, numberResult: results.length });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getCommentNew = (req, res) => {
  const { idCommentNews } = req.body;
  const sql = `SELECT * FROM commentNews WHERE idCommentNews = ${idCommentNews}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const deleteCommentNews = (req, res) => {
  const { idCommentNews } = req.body;
  const sql = `DELETE FROM commentNews WHERE idCommentNews = ${idCommentNews}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Comment Deleted' });
  });
};

const updateCommentNews = (req, res) => {
  const { idCommentNews } = req.body;
  const dataObj = req.body;
  const sql = `UPDATE commentNews SET ? WHERE idCommentNews = ${idCommentNews}`;
  conexion.query(sql, dataObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Comment updated' });
  });
};

module.exports = {
  createCommentNews,
  deleteCommentNews,
  getCommentNew,
  getCommentsNews,
  updateCommentNews,
};
