const conexion = require('../../sql/conexion');

const createFiltersByAge = async (req, res) => {
  const sql = 'INSERT INTO filtersByAge SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Age Create!' });
  });
};

const deleteFiltersByAge = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersByAge WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Age Deleted' });
  });
};

const getFiltersByAges = (req, res) => {
  const sql = 'SELECT * FROM filtersByAge';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersByAges = (req, res) => {
  const sql = 'SELECT keyword AS `value`, name AS `label` FROM filtersByAge';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersByAge = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersByAge WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersByAge = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersByAge SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Age updated' });
  });
};

module.exports = {
  createFiltersByAge,
  deleteFiltersByAge,
  getFiltersByAge,
  getFiltersByAges,
  consulteFiltersByAges,
  updateFiltersByAge,
};
