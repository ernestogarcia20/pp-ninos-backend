const conexion = require('../../sql/conexion');

const createFiltersBySpecialist = async (req, res) => {
  const sql = 'INSERT INTO filtersBySpecialist SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Specialist Create!' });
  });
};

const deleteFiltersBySpecialist = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersBySpecialist WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Specialist Deleted' });
  });
};

const getFiltersBySpecialists = (req, res) => {
  const sql = 'SELECT * FROM filtersBySpecialist';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersBySpecialists = (req, res) => {
  const sql =
    'SELECT keyword AS `value`, name AS `label` FROM filtersBySpecialist';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersBySpecialist = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersBySpecialist WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersBySpecialist = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersBySpecialist SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Specialist updated' });
  });
};

module.exports = {
  createFiltersBySpecialist,
  deleteFiltersBySpecialist,
  getFiltersBySpecialist,
  getFiltersBySpecialists,
  consulteFiltersBySpecialists,
  updateFiltersBySpecialist,
};
