const conexion = require('../../sql/conexion');

const createTypePlan = async (req, res) => {
  const sql = 'INSERT INTO type_plans SET ?';
  const userObj = {
    id: req.body.id,
    name: req.body.name,
  };
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send('Type plans Create!');
  });
};

const deleteTypePlan = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM type_plans WHERE id = '${id}'`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Type Plan deleted' });
  });
};

const getTypesPlan = (req, res) => {
  const sql = 'SELECT * FROM type_plans';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getTypePlan = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM type_plans WHERE id = '${id}'`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateTypePlan = (req, res) => {
  const { id } = req.body;
  const sql = `UPDATE type_plans SET name = '${req.body.name}' WHERE id = '${id}'`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Type Plan updated' });
  });
};

module.exports = {
  createTypePlan,
  deleteTypePlan,
  getTypesPlan,
  getTypePlan,
  updateTypePlan,
};
