const conexion = require('../../sql/conexion');

const createFiltersByLocation = async (req, res) => {
  const sql = 'INSERT INTO filtersByLocation SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Location Create!' });
  });
};

const deleteFiltersByLocation = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersByLocation WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Location Deleted' });
  });
};

const getFiltersByLocations = (req, res) => {
  const sql = 'SELECT * FROM filtersByLocation';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersByLocations = (req, res) => {
  const sql =
    'SELECT keyword AS `value`, name AS `label` FROM filtersByLocation';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersByLocation = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersByLocation WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersByLocation = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersByLocation SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Location updated' });
  });
};

module.exports = {
  createFiltersByLocation,
  deleteFiltersByLocation,
  getFiltersByLocation,
  getFiltersByLocations,
  consulteFiltersByLocations,
  updateFiltersByLocation,
};
