const conexion = require('../../sql/conexion');

const createNews = (req, res) => {
  const sql = 'INSERT INTO news SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'News Create!' });
  });
};

const editNews = (req, res) => {
  const { idNews } = req.body;
  const userObj = req.body;
  const sql = `UPDATE news SET ? WHERE idNews = '${idNews}'`;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'News updated' });
  });
};

const deleteNews = (req, res) => {
  const { idNews } = req.body;
  const sql = `DELETE FROM news WHERE idNews = '${idNews}'`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'News deleted' });
  });
};

const getNews = (req, res) => {
  const sql = 'SELECT * FROM news ORDER BY updateDateNews DESC';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', numberResult: results.length, data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getPageNews = (req, res) => {
  const { limit, offSet } = req.query;
  const sql = 'SELECT * FROM news ORDER BY updateDateNews DESC';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      const newsql = `SELECT * FROM news ORDER BY updateDateNews DESC LIMIT ${offSet},${limit}`;
      conexion.query(newsql, (e, r) => {
        if (e) throw e;
        if (r.length > 0) {
          res.send({
            status: 'OK',
            resultNumber: results.length,
            numberPages: Math.ceil(results.length / limit),
            data: r,
          });
        } else {
          res
            .status(401)
            .send({ status: 'NOT DATA', data: 'no results found' });
        }
      });
    }
  });
};

const getNew = (req, res) => {
  const { idNews } = req.body;
  const sql = `SELECT * FROM news WHERE idNews = '${idNews}'`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getNewForName = (req, res) => {
  const { nameNews } = req.body;
  const sql = `SELECT * FROM news WHERE seoKeyword = '${nameNews}'`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getNewConsult = (req, res) => {
  const { idNews } = req.body;
  const sql = `SELECT news.*, customersUsers.idCustomer, customersUsers.firstName, customersUsers.lastName FROM news, customersUsers WHERE idNews = '${idNews}' AND customersUsers.idCustomer = news.idCustomer`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getCosultNews = (req, res) => {
  const sql =
    'SELECT `news`.*, `customersUsers`.`idCustomer`, `customersUsers`.`firstName`, `customersUsers`.`lastName`, `customersUsers`.`email`, `customersUsers`.`phone` FROM `news`, `customersUsers` WHERE `customersUsers`.`idCustomer` = `news`.`idCustomer` ORDER BY news.updateDateNews DESC';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', numberResult: results.length, data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

module.exports = {
  createNews,
  editNews,
  deleteNews,
  getNews,
  getNew,
  getCosultNews,
  getNewConsult,
  getPageNews,
  getNewForName,
};
