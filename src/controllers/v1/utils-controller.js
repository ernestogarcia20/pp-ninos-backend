const keyGenerator = (req, res) => {
  try {
    const all = 'abcdefghijklmnopqrstuvwxyz1234567890$%@#*';
    let passFinal = '';
    for (let i = 0; i < 10; i += 1) {
      const ran = Math.floor(Math.random() * all.length);
      passFinal += all[ran];
    }
    if (passFinal.length === 10) {
      res.send({ status: 'OK', password: passFinal });
    } else {
      res
        .status(204)
        .send({ status: 'NOT DATA', password: 'no results found' });
    }
  } catch (error) {
    res.status(500).send({ status: 'ERROR', message: error.message });
  }
};

module.exports = { keyGenerator };
