const conexion = require('../../sql/conexion');

const createStatus = async (req, res) => {
  const sql = 'INSERT INTO statuses SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Status Create!' });
  });
};

const deleteStatus = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM statuses WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Status Deleted' });
  });
};

const getStatuses = (req, res) => {
  const sql = 'SELECT * FROM statuses';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteStatuses = (req, res) => {
  const sql = 'SELECT name AS `value`, name AS `label` FROM statuses';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getStatus = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM statuses WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateStatus = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;

  const sql = `UPDATE statuses SET ${name} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Status updated' });
  });
};

module.exports = {
  createStatus,
  deleteStatus,
  getStatus,
  getStatuses,
  consulteStatuses,
  updateStatus,
};
