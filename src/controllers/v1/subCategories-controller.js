const conexion = require('../../sql/conexion');

const createSubCategorie = async (req, res) => {
  const sql = 'INSERT INTO subCategory SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Categorie Create!' });
  });
};

const deleteSubCategorie = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM subCategory WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Categorie Deleted' });
  });
};

const getSubCategories = (req, res) => {
  const sql = 'SELECT * FROM subCategory';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteSubCategories = (req, res) => {
  const sql =
    'SELECT nameSubCategory AS `value`, nameSubCategory AS `label` FROM subCategory';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteSubCategoriesFromCategories = (req, res) => {
  const { idCategories } = req.body;
  const sql = `SELECT nameSubCategory AS 'value', nameSubCategory AS 'label' FROM subCategory WHERE id_category IN (${idCategories})`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getSubCategorie = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM subCategory WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateSubCategorie = (req, res) => {
  const { id } = req.body;
  const dataObj = req.body;

  const sql = `UPDATE subCategory SET ? WHERE id = ${id}`;

  conexion.query(sql, dataObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Category updated' });
  });
};

const getSubCategoriesAndPost = (req, res) => {
  const newsql =
    'SELECT publications.id AS `codePost`, publications.seoKeyword, subCategory.nameSubCategory FROM publications, subCategory WHERE FIND_IN_SET(subCategory.nameSubCategory,publications.subCategories) ORDER BY publications.seoKeyword ASC';
  conexion.query(newsql, (e, result) => {
    if (e) throw e;
    if (result.length > 0) {
      res.send({
        status: 'OK',
        totalResult: result.length,
        result,
      });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

module.exports = {
  createSubCategorie,
  deleteSubCategorie,
  getSubCategorie,
  getSubCategories,
  updateSubCategorie,
  consulteSubCategories,
  consulteSubCategoriesFromCategories,
  getSubCategoriesAndPost,
};
