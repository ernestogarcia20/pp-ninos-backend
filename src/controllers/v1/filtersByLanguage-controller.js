const conexion = require('../../sql/conexion');

const createFiltersByLanguage = async (req, res) => {
  const sql = 'INSERT INTO filtersByLanguage SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Language Create!' });
  });
};

const deleteFiltersByLanguage = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersByLanguage WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Language Deleted' });
  });
};

const getFiltersByLanguages = (req, res) => {
  const sql = 'SELECT * FROM filtersByLanguage';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersByLanguages = (req, res) => {
  const sql =
    'SELECT keyword AS `value`, name AS `label` FROM filtersByLanguage';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersByLanguage = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersByLanguage WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersByLanguage = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersByLanguage SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Language updated' });
  });
};

module.exports = {
  createFiltersByLanguage,
  deleteFiltersByLanguage,
  getFiltersByLanguage,
  getFiltersByLanguages,
  consulteFiltersByLanguages,
  updateFiltersByLanguage,
};
