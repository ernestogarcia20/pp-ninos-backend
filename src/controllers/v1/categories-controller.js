const conexion = require('../../sql/conexion');

const createCategorie = async (req, res) => {
  const sql = 'INSERT INTO categories SET ?';
  const userObj = {
    name: req.body.name,
    meta_tag_description: req.body.metaTagDescription,
    meta_tag_keyword: req.body.metaTagKeyword,
    description: req.body.description,
  };
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Categorie Create!' });
  });
};

const deleteCategorie = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM categories WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Categorie Deleted' });
  });
};

const getCategories = (req, res) => {
  const sql = 'SELECT * FROM categories ORDER BY positionCategory ASC';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteCategories = (req, res) => {
  const sql =
    'SELECT name AS `value`, name AS `label`, id AS `idCategory` FROM categories';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getCategorie = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM categories WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateCategorie = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const metaTagDescription = `meta_tag_description='${req.body.metaTagDescription}', `;
  const metaTagKeyword = `meta_tag_keyword='${req.body.metaTagKeyword}', `;
  const description = `description='${req.body.description}'`;

  const queryVariables = `${name}${metaTagDescription}${metaTagKeyword}${description}`;

  const sql = `UPDATE categories SET ${queryVariables} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Category updated' });
  });
};

const getCategoriesAndPost = (req, res) => {
  const newsql =
    'SELECT publications.id AS `codePost`, publications.seoKeyword, categories.name FROM publications, categories WHERE FIND_IN_SET(categories.name,publications.category) ORDER BY publications.seoKeyword ASC';
  conexion.query(newsql, (e, result) => {
    if (e) throw e;
    if (result.length > 0) {
      res.send({
        status: 'OK',
        totalResult: result.length,
        result,
      });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

module.exports = {
  createCategorie,
  deleteCategorie,
  getCategorie,
  getCategories,
  updateCategorie,
  consulteCategories,
  getCategoriesAndPost,
};
