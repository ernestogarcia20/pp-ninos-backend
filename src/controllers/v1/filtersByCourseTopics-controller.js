const conexion = require('../../sql/conexion');

const createFiltersByCourseTopic = async (req, res) => {
  const sql = 'INSERT INTO filtersByCourseTopics SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Course Topics Create!' });
  });
};

const deleteFiltersByCourseTopic = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersByCourseTopics WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Course Topics Deleted' });
  });
};

const getFiltersByCourseTopics = (req, res) => {
  const sql = 'SELECT * FROM filtersByCourseTopics';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersByCourseTopics = (req, res) => {
  const sql =
    'SELECT keyword AS `value`, name AS `label` FROM filtersByCourseTopics';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersByCourseTopic = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersByCourseTopics WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersByCourseTopic = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersByCourseTopics SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Course Topics updated' });
  });
};

module.exports = {
  createFiltersByCourseTopic,
  deleteFiltersByCourseTopic,
  getFiltersByCourseTopic,
  getFiltersByCourseTopics,
  consulteFiltersByCourseTopics,
  updateFiltersByCourseTopic,
};
