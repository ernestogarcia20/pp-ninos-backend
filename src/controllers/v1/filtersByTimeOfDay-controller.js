const conexion = require('../../sql/conexion');

const createFiltersByTimeOfDay = async (req, res) => {
  const sql = 'INSERT INTO filtersByTimeOfDay SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By TimeOfDay Create!' });
  });
};

const deleteFiltersByTimeOfDay = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersByTimeOfDay WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By TimeOfDay Deleted' });
  });
};

const getFiltersByTimeOfDays = (req, res) => {
  const sql = 'SELECT * FROM filtersByTimeOfDay';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersByTimeOfDays = (req, res) => {
  const sql =
    'SELECT keyword AS `value`, name AS `label` FROM filtersByTimeOfDay';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersByTimeOfDay = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersByTimeOfDay WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersByTimeOfDay = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersByTimeOfDay SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By TimeOfDay updated' });
  });
};

module.exports = {
  createFiltersByTimeOfDay,
  deleteFiltersByTimeOfDay,
  getFiltersByTimeOfDay,
  getFiltersByTimeOfDays,
  consulteFiltersByTimeOfDays,
  updateFiltersByTimeOfDay,
};
