/* eslint-disable no-throw-literal */
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const conexion = require('../../sql/conexion');

const expiresIn = 60 * 60 * 8;

function enviaMail(email, password, idUser) {
  const transport = nodemailer.createTransport({
    host: 'mail.panamaparaninos.com',
    post: 465,
    secure: true,
    auth: {
      user: 'register@panamaparaninos.com',
      pass: 'Panama2021.',
    },
  });
  const mailOptions = {
    from: 'register@panamaparaninos.com',
    to: `${email}`,
    subject: 'Clave de Usuario Panamá para Niños',
    text: `Adjunto envio el password generado automaticamente para el usario ${idUser}, Password: ${password}`,
  };
  transport.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error.message);
    } else {
      console.log(info.response);
    }
  });
}

const createUser = async (req, res) => {
  try {
    const hash = await bcrypt.hash(req.body.password, 15);
    const sql = 'INSERT INTO customersUsers SET ?';
    const date = Date.now();
    const userObj = {
      ...req.body,
      password: hash,
      dateAdded: date,
    };
    conexion.query(sql, userObj, (error) => {
      if (error) throw error;
      res.send({ status: 'OK', message: 'user created' });
      enviaMail(req.body.email, req.body.password, req.body.user);
    });
  } catch (error) {
    res.status(500).send({ status: 'ERROR', message: error.message });
  }
};

const deleteUser = (req, res) => {
  const { idCustomer } = req.body;
  const sql = `DELETE FROM customersUsers WHERE idCustomer = '${idCustomer}'`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'user deleted' });
  });
};

const getUsers = (req, res) => {
  const sql = 'SELECT * FROM customersUsers';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteUsers = (req, res) => {
  // eslint-disable-next-line quotes
  const sql = `SELECT idCustomer AS 'value', concat_ws('',firstName,' ',lastName) AS 'label' FROM customersUsers`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getUser = (req, res) => {
  const { idCustomer } = req.body;
  const sql = `SELECT * FROM customersUsers WHERE idCustomer = '${idCustomer}'`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateUser = (req, res) => {
  const { idCustomer } = req.body;
  const dataObj = req.body;

  const sql = `UPDATE customersUsers SET ? WHERE idCustomer = '${idCustomer}'`;

  conexion.query(sql, dataObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'user updated' });
  });
};

const statusToken = (req, res) => {
  try {
    const { token } = req.body;
    if (token) {
      const status = jwt.verify(token, process.env.JWT_SECRET);
      if (status.exp > status.iat) {
        res.status(200).send({ status: 'OK', message: 'TODO BIEN' });
      } else {
        res
          .status(403)
          .send({ status: 'ACCESS_DENIED', message: 'Missing header token' });
      }
    } else {
      res
        .status(403)
        .send({ status: 'ACCESS_DENIED', message: 'Missing header token' });
    }
  } catch (e) {
    res
      .status(e.code || 500)
      .send({ status: e.status || 'ERROR', message: e.message });
  }
};

const loginUser = (req, res) => {
  const { email, password } = req.body;
  const sql = `SELECT * FROM customersUsers WHERE email = '${email}'`;
  conexion.query(sql, async (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      bcrypt.compare(password, results[0].password, (err, result) => {
        if (result) {
          const token = jwt.sign(
            { userId: results[0].user, role: results[0].typeUser },
            process.env.JWT_SECRET,
            { expiresIn }
          );
          res.send({
            status: 'OK',
            data: {
              token,
              expiresIn,
              info: results[0],
              grupCustomer: results[0].idGroupCustomer,
              userID: results[0].idCustomer,
            },
          });
        } else if (err) {
          res.status(500).send({ status: 'ERROR', message: err.message });
        } else {
          res.status(403).send({
            status: 'INVALID_PASSWORD',
            message: 'the password you are using is not correct',
          });
        }
      });
    } else {
      res.status(401).send({
        status: 'USER_NOT_FOUND',
        message:
          'The user you are trying to access does not exist, check and try again',
      });
    }
  });
};

const multiConsulteCustomers = (req, res) => {
  const sql =
    'SELECT * FROM customersUsers, groupCustomers WHERE groupCustomers.idgroupcustomer = customersUsers.idGroupCustomer';
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

module.exports = {
  createUser,
  deleteUser,
  getUser,
  getUsers,
  consulteUsers,
  updateUser,
  loginUser,
  multiConsulteCustomers,
  statusToken,
};
