const conexion = require('../../sql/conexion');

const createPlan = async (req, res) => {
  const sql = 'INSERT INTO plans SET ?';
  const userObj = {
    idplan: req.body.idplan,
    nameplan: req.body.name,
    price: req.body.price,
    id_type_plan: req.body.id_type_plan,
  };
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send('Type plans Create!');
  });
};

const deletePlan = (req, res) => {
  const { idplan } = req.body;
  const sql = `DELETE FROM plans WHERE idplan = ${idplan}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Type Plan deleted' });
  });
};

const getPlans = (req, res) => {
  const sql = 'SELECT * FROM plans';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consultePlans = (req, res) => {
  const sql = 'SELECT idplan AS `value`, nameplan AS `label` FROM plans';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getPlan = (req, res) => {
  const { idplan } = req.body;
  console.log(idplan);
  const sql = `SELECT * FROM plans WHERE idplan = ${idplan}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updatePlan = (req, res) => {
  const { idplan } = req.body;

  let name = '';
  let price = '';
  let idTypePlan = '';

  if (req.body.id_type_plan && req.body.id_type_plan.length > 0) {
    idTypePlan = `_id-type-plan = '${req.body.id_type_plan}'?`;
  }
  if (req.body.price && req.body.price.length > 0) {
    price = `_price = '${req.body.price}'?`;
  }
  if (req.body.name && req.body.name.length > 0) {
    name = `_nameplan = '${req.body.nameplan}'?`;
  }

  const queryVariables = `${name}${price}${idTypePlan}`;
  const subresult1 = queryVariables.replace(/\?_/g, ',');
  const subresult2 = subresult1.replace('?', '');
  const subresult3 = subresult2.replace('_', '');

  const sql = `UPDATE plans SET ${subresult3} WHERE idplan = ${idplan}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Type Plan updated' });
  });
};

module.exports = {
  createPlan,
  deletePlan,
  getPlan,
  getPlans,
  consultePlans,
  updatePlan,
};
