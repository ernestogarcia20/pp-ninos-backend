const conexion = require('../../sql/conexion');

const createComment = async (req, res) => {
  const sql = 'INSERT INTO commentary SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Comment Create!' });
  });
};

const getComments = (req, res) => {
  const { idPost } = req.body;
  const sql = `SELECT * FROM commentary WHERE id_post = '${idPost}'`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results, numberResult: results.length });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getComment = (req, res) => {
  const { idComment } = req.body;
  const sql = `SELECT * FROM commentary WHERE idComment = ${idComment}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const deleteComment = (req, res) => {
  const { idComment } = req.body;
  const sql = `DELETE FROM commentary WHERE idComment = ${idComment}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Comment Deleted' });
  });
};

const updateComment = (req, res) => {
  const { idComment } = req.body;
  const dataObj = req.body;
  const sql = `UPDATE commentary SET ? WHERE idComment = ${idComment}`;
  conexion.query(sql, dataObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Comment updated' });
  });
};

module.exports = {
  createComment,
  deleteComment,
  getComment,
  getComments,
  updateComment,
};
