const conexion = require('../../sql/conexion');

const createCountry = async (req, res) => {
  const sql = 'INSERT INTO countries SET ?';
  const userObj = {
    name: req.body.name,
    ISOCode2: req.body.ISOCode2,
    ISOCode3: req.body.ISOCode3,
    status: req.body.status,
  };
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Country Create!' });
  });
};

const deleteCountry = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM countries WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Country Deleted' });
  });
};

const getCountries = (req, res) => {
  const sql = 'SELECT * FROM countries ORDER BY name';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteCountries = (req, res) => {
  const sql =
    'SELECT name AS `value`, name AS `label` FROM countries ORDER BY name';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getCountry = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM countries WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateCountry = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const isoCode2 = `ISOCode2='${req.body.ISOCode2}', `;
  const isoCode3 = `ISOCode3='${req.body.ISOCode3}', `;
  const status = `status='${req.body.status}'`;

  const queryVariables = `${name}${isoCode2}${isoCode3}${status}`;

  const sql = `UPDATE countries SET ${queryVariables} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Country updated' });
  });
};

module.exports = {
  createCountry,
  deleteCountry,
  getCountry,
  getCountries,
  consulteCountries,
  updateCountry,
};
