const conexion = require('../../sql/conexion');

const createProvince = async (req, res) => {
  const sql = 'INSERT INTO province SET ?';
  const userObj = {
    name: req.body.name,
    country: req.body.country,
  };
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Province Create!' });
  });
};

const deleteProvince = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM province WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Province Deleted' });
  });
};

const getProvinces = (req, res) => {
  const sql = 'SELECT * FROM province ORDER BY name';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteProvinces = (req, res) => {
  const sql =
    'SELECT name AS `value`, name AS `label` FROM province ORDER BY name';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getProvince = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM province WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateProvince = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const country = `country='${req.body.country}'`;

  const queryVariables = `${name}${country}`;

  const sql = `UPDATE province SET ${queryVariables} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Province updated' });
  });
};

const consulteProvinceForCountry = (req, res) => {
  const { country } = req.body;
  const sql = `SELECT name AS 'value', name AS 'label' FROM province WHERE country = '${country}' ORDER BY name`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

module.exports = {
  createProvince,
  deleteProvince,
  getProvince,
  getProvinces,
  consulteProvinces,
  updateProvince,
  consulteProvinceForCountry,
};
