const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const conexion = require('../../sql/conexion');

const expiresIn = 60 * 60 * 8;

function enviaMail(email, password, idUser) {
  const transport = nodemailer.createTransport({
    host: 'mail.galexsolution.com',
    post: 465,
    secure: true,
    auth: {
      user: 'pruebas@galexsolution.com',
      pass: 'C0l0mb14$2020.',
    },
  });
  const mailOptions = {
    from: 'register@panamaparaninos.com',
    to: `${email}; gustavo.barrios@galexsolution.com`,
    subject: 'Clave de Usuario Panamá para Niños',
    text: `Adjunto envio el password generado automaticamente para el usario ${idUser}, Password: ${password}`,
  };
  transport.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error.message);
    } else {
      console.log(info.response);
    }
  });
}

const createUser = async (req, res) => {
  try {
    const all = 'abcdefghijklmnopqrstuvwxyz1234567890$%@#*';
    let passFinal = '';
    for (let i = 0; i < 10; i += 1) {
      const ran = Math.floor(Math.random() * all.length);
      passFinal += all[ran];
    }
    enviaMail(req.body.email, passFinal, req.body.user);
    const hash = await bcrypt.hash(passFinal, 15);
    const sql = 'INSERT INTO users SET ?';
    const userObj = {
      user: req.body.user,
      contactName: req.body.contactName,
      email: req.body.email,
      phone: req.body.phone,
      provincia: req.body.provincia,
      pais: req.body.pais,
      typeUser: req.body.typeUser,
      password: hash,
    };
    conexion.query(sql, userObj, (error) => {
      if (error) throw error;
      res.send({ status: 'OK', message: 'user created' });
    });
  } catch (error) {
    res.status(500).send({ status: 'ERROR', message: error.message });
  }
};

const deleteUser = (req, res) => {
  const { userID } = req.body;
  const sql = `DELETE FROM users WHERE user = '${userID}'`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'user deleted' });
  });
};

const getUsers = (req, res) => {
  const sql = 'SELECT * FROM users';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteUsers = (req, res) => {
  const sql = 'SELECT user AS `value`, companyName AS `label` FROM users';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getUser = (req, res) => {
  const { userID } = req.body;
  const sql = `SELECT * FROM users WHERE user = '${userID}'`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateUser = (req, res) => {
  const { userID } = req.body;
  let queryName = '';
  let queryEmail = '';
  let queryPhone = '';
  let queryProvincia = '';
  let queryCountry = '';
  let queryTypeUser = '';
  let queryCName = '';
  let QueryIdCompany = '';
  let queryPassword = '';

  if (req.body.contactName && req.body.contactName.length > 0) {
    queryName = `_contactName = '${req.body.contactName}'?`;
  }
  if (req.body.email && req.body.email.length > 0) {
    queryEmail = `_email = '${req.body.email}'?`;
  }
  if (req.body.phone && req.body.phone.length > 0) {
    queryPhone = `_phone = '${req.body.phone}'?`;
  }
  if (req.body.provincia && req.body.provincia.length > 0) {
    queryProvincia = `_provincia = '${req.body.provincia}'?`;
  }
  if (req.body.pais && req.body.pais.length > 0) {
    queryCountry = `_pais = '${req.body.pais}'?`;
  }
  if (req.body.typeUser && req.body.typeUser.length > 0) {
    queryTypeUser = `_typeUser = '${req.body.typeUser}'?`;
  }
  if (req.body.companyName && req.body.companyName.length > 0) {
    queryCName = `_companyName = '${req.body.companyName}'?`;
  }
  if (req.body.idCompany && req.body.idCompany.length > 0) {
    QueryIdCompany = `_idCompany = '${req.body.idCompany}'?`;
  }
  if (req.body.password && req.body.password.length > 0) {
    queryPassword = `_password = '${req.body.password}'?`;
  }

  const queryVariables = `${queryName}${queryEmail}${queryPhone}${queryProvincia}${queryCountry}${queryTypeUser}${queryCName}${QueryIdCompany}${queryPassword}`;

  const subresult1 = queryVariables.replace(/\?_/g, ',');
  const subresult2 = subresult1.replace('?', '');
  const subresult3 = subresult2.replace('_', '');

  const sql = `UPDATE users SET ${subresult3} WHERE user = '${userID}'`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'user updated' });
  });
};

const loginUser = (req, res) => {
  const { userID, password } = req.body;
  const sql = `SELECT * FROM users WHERE user = '${userID}'`;
  conexion.query(sql, async (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      bcrypt.compare(password, results[0].password, (err, result) => {
        if (result) {
          const token = jwt.sign(
            { userId: results[0].user, role: results[0].typeUser },
            process.env.JWT_SECRET,
            { expiresIn }
          );
          res.send({ status: 'OK', data: { token, expiresIn } });
        } else if (err) {
          res.status(500).send({ status: 'ERROR', message: err.message });
        } else {
          res.status(403).send({
            status: 'INVALID_PASSWORD',
            message: 'the password you are using is not correct',
          });
        }
      });
    } else {
      res.status(401).send({
        status: 'USER_NOT_FOUND',
        message:
          'The user you are trying to access does not exist, check and try again',
      });
    }
  });
};

module.exports = {
  createUser,
  deleteUser,
  getUser,
  getUsers,
  consulteUsers,
  updateUser,
  loginUser,
};
