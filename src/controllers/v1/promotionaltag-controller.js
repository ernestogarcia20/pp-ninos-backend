const conexion = require('../../sql/conexion');

const createPromotionalTag = async (req, res) => {
  const sql = 'INSERT INTO promotionalTag SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'PromotionalTag Create!' });
  });
};

const deletePromotionalTag = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM promotionalTag WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'PromotionalTag Deleted' });
  });
};

const getPromotionalTags = (req, res) => {
  const sql = 'SELECT * FROM promotionalTag';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consultePromotionalTags = (req, res) => {
  const sql = 'SELECT name AS `value`, name AS `label` FROM promotionalTag';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getPromotionalTag = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM promotionalTag WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getTagForName = (req, res) => {
  const { name } = req.body;
  const sql = `SELECT * FROM promotionalTag WHERE name = '${name}'`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(404).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updatePromotionalTag = (req, res) => {
  const { id } = req.body;
  const dataObj = req.body;

  const sql = `UPDATE promotionalTag SET ? WHERE id = ${id}`;

  conexion.query(sql, dataObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'PromotionalTag updated' });
  });
};

module.exports = {
  createPromotionalTag,
  deletePromotionalTag,
  getPromotionalTag,
  getPromotionalTags,
  consultePromotionalTags,
  updatePromotionalTag,
  getTagForName,
};
