const conexion = require('../../sql/conexion');

const createZone = async (req, res) => {
  const sql = 'INSERT INTO zone SET ?';
  const userObj = {
    name: req.body.name,
    city: req.body.city,
  };
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Zone Create!' });
  });
};

const deleteZone = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM zone WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Zone Deleted' });
  });
};

const getZones = (req, res) => {
  const sql = 'SELECT * FROM zone ORDER BY name';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteZones = (req, res) => {
  const sql = 'SELECT name AS `value`, name AS `label` FROM zone ORDER BY name';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteZonesForDistrict = (req, res) => {
  const { district } = req.body;
  const sql = `SELECT name AS 'value', name AS 'label' FROM zone WHERE city = '${district}' ORDER BY name`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getZone = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM zone WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteZone = (req, res) => {
  const { name } = req.body;
  const sql = `SELECT city.name AS 'district', province.name AS 'province', countries.name AS 'country' FROM zone, city, province, countries WHERE zone.name='${name}' AND zone.city=city.name AND city.province=province.name AND province.country=countries.name ORDER BY name`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateZone = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const city = `city='${req.body.city}'`;

  const queryVariables = `${name}${city}`;

  const sql = `UPDATE zone SET ${queryVariables} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Zone updated' });
  });
};

module.exports = {
  createZone,
  deleteZone,
  getZone,
  consulteZone,
  getZones,
  consulteZones,
  updateZone,
  consulteZonesForDistrict,
};
