const conexion = require('../../sql/conexion');

const createFiltersBySport = async (req, res) => {
  const sql = 'INSERT INTO filtersBySport SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Sport Create!' });
  });
};

const deleteFiltersBySport = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersBySport WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Sport Deleted' });
  });
};

const getFiltersBySports = (req, res) => {
  const sql = 'SELECT * FROM filtersBySport';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersBySports = (req, res) => {
  const sql = 'SELECT keyword AS `value`, name AS `label` FROM filtersBySport';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersBySport = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersBySport WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersBySport = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersBySport SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Sport updated' });
  });
};

module.exports = {
  createFiltersBySport,
  deleteFiltersBySport,
  getFiltersBySport,
  getFiltersBySports,
  consulteFiltersBySports,
  updateFiltersBySport,
};
