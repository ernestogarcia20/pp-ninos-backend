const os = require('os');
const conexion = require('../../sql/conexion');

const ifaces = os.networkInterfaces();
let addressIP = '';
let addressIP1 = '';

Object.keys(ifaces).forEach((ifname) => {
  let alias = 0;

  ifaces[ifname].forEach((iface) => {
    if (iface.family !== 'IPv4' || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }

    if (alias >= 1) {
      // this single interface has multiple ipv4 addresses
      console.log(`${ifname}:${alias}`, iface.address);
      if (ifname === 'eth0') {
        addressIP1 = iface.address;
      } else if (ifname === 'en0') {
        addressIP1 = iface.address;
      }
    } else {
      // this interface has only one ipv4 adress
      console.log(ifname, iface.address);
      if (ifname === 'eth0') {
        addressIP = iface.address;
      } else if (ifname === 'en0') {
        addressIP = iface.address;
      }
    }
    ++alias;
  });
});

console.log(addressIP);
console.log(addressIP1);

const createPublications = (req, res) => {
  const sql = 'INSERT INTO publications SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Publications Create!' });
  });
};

const editPublications = (req, res) => {
  const { id } = req.body;
  const userObj = req.body;

  const sql = `UPDATE publications SET ? WHERE id = '${id}'`;

  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Publications updated' });
  });
};

const deletePublications = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM publications WHERE id = '${id}'`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Publications deleted' });
  });
};

const getPublications = (req, res) => {
  const { filtro } = req.query;
  const sql = `SELECT * FROM publications ${filtro} ORDER BY orden,numberViews DESC`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', numberResult: results.length, data: results });
    } else {
      res.status(404).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getPublicationsAndPromotionalTag = (req, res) => {
  const sql =
    'SELECT publications.*, promotionalTag.ImagePromotionalTag FROM publications , promotionalTag WHERE promotionalTag.name=publications.promotionalTag AND idPostAssociated = "" AND idPlan IN (100300,100301,100302,100400) ORDER BY orden,numberViews DESC';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', numberResult: results.length, data: results });
    } else {
      res.status(404).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getPublicationsBranch = (req, res) => {
  const validation = 'yes';
  const sql = `SELECT name AS 'label', id AS 'value' FROM publications WHERE branch = '${validation}' ORDER BY orden,numberViews DESC`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', numberResult: results.length, data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getBranchPublications = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM publications WHERE idPostAssociated = '${id}' ORDER BY orden,numberViews DESC`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', numberResult: results.length, data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getPublicationsForCustomer = (req, res) => {
  const { id } = req.body;
  const { filter } = req.body;
  const sql = `SELECT * FROM publications WHERE idUser = '${id}' ${filter} ORDER BY orden,numberViews DESC`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', numberResult: results.length, data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const multiGetPublications = (req, res) => {
  const sql =
    'SELECT * FROM publications, customersUsers, plans WHERE customersUsers.idCustomer = publications.idUser AND plans.idplan = publications.idPlan ORDER BY orden,numberViews DESC';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', numberResult: results.length, data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const multiGetPublicationsFilter = (req, res) => {
  const { name } = req.query;
  const sql = `SELECT * FROM publications, customersUsers, plans WHERE publications.name LIKE '%${name}%' AND customersUsers.idCustomer = publications.idUser AND plans.idplan = publications.idPlan ORDER BY orden,numberViews DESC`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', numberResult: results.length, data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getPagePublications = (req, res) => {
  const { table, limit, offSet } = req.query;
  const filter = req.query.filter ? req.query.filter : '%';
  const sql = `SELECT * FROM publications WHERE statusPost = 'activated' AND idPostAssociated = "" AND ${table} LIKE '${filter}'`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      const newsql = `SELECT * FROM publications WHERE statusPost = 'activated' AND idPostAssociated = "" AND ${table} LIKE '${filter}' ORDER BY (CASE WHEN (idPlan = 100300 OR idPlan = 100301 OR idPlan = 100302 OR idPlan = 100400) THEN 1 ELSE 2 END),orden,numberViews DESC LIMIT ${offSet},${limit}`;
      conexion.query(newsql, (e, r) => {
        if (e) throw e;
        if (r.length > 0) {
          res.send({
            status: 'OK',
            resultNumber: results.length,
            numberPages: Math.ceil(results.length / limit),
            data: r,
          });
        } else {
          res
            .status(401)
            .send({ status: 'NOT DATA', data: 'no results found' });
        }
      });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFilterPublications = (req, res) => {
  const limit = 10;
  let page = 0;
  let offSet = 0;
  let next;
  let previous;
  let nextUrl;
  let previousUrl;
  const filter = req.query.filter ? req.query.filter : '%';
  const { table } = req.query;
  const filter1 = req.query.filter1
    ? `AND ${req.query.tableFilter1} LIKE '${req.query.filter1}'`
    : '';
  const filter2 = req.query.filter2
    ? `AND ${req.query.tableFilter2} LIKE '${req.query.filter2}'`
    : '';
  const filter3 = req.query.filter3
    ? `AND ${req.query.tableFilter3} LIKE '${req.query.filter3}'`
    : '';
  const filter4 = req.query.filter4
    ? `AND ${req.query.tableFilter4} LIKE '${req.query.filter4}'`
    : '';
  const filter5 = req.query.filter5
    ? `AND ${req.query.tableFilter5} LIKE '${req.query.filter5}'`
    : '';
  const filter6 = req.query.filter6
    ? `AND ${req.query.tableFilter6} LIKE '${req.query.filter6}'`
    : '';
  const filter7 = req.query.filter7
    ? `AND ${req.query.tableFilter7} LIKE '${req.query.filter7}'`
    : '';
  const filter8 = req.query.filter8
    ? `AND ${req.query.tableFilter8} LIKE '${req.query.filter8}'`
    : '';
  const filter9 = req.query.filter9
    ? `AND ${req.query.tableFilter9} LIKE '${req.query.filter9}'`
    : '';
  const filter10 = req.query.filter10
    ? `AND ${req.query.tableFilter10} LIKE '${req.query.filter10}'`
    : '';
  const filter11 = req.query.filter11
    ? `AND ${req.query.tableFilter11} LIKE '${req.query.filter11}'`
    : '';
  const filter12 = req.query.filter12
    ? `AND ${req.query.tableFilter12} LIKE '${req.query.filter12}'`
    : '';
  const filter13 = req.query.filter13
    ? `AND ${req.query.tableFilter13} LIKE '${req.query.filter13}'`
    : '';
  const filter14 = req.query.filter14
    ? `AND ${req.query.tableFilter14} LIKE '${req.query.filter14}'`
    : '';

  const enlace1 = req.query.filter1
    ? `&tableFilter1=${req.query.tableFilter1}&filter1=${req.query.filter1}`
    : '';
  const enlace2 = req.query.filter2
    ? `&tableFilter2=${req.query.tableFilter2}&filter2=${req.query.filter2}`
    : '';
  const enlace3 = req.query.filter3
    ? `&tableFilter3=${req.query.tableFilter3}&filter3=${req.query.filter3}`
    : '';
  const enlace4 = req.query.filter4
    ? `&tableFilter4=${req.query.tableFilter4}&filter4=${req.query.filter4}`
    : '';
  const enlace5 = req.query.filter5
    ? `&tableFilter5=${req.query.tableFilter5}&filter5=${req.query.filter5}`
    : '';
  const enlace6 = req.query.filter6
    ? `&tableFilter6=${req.query.tableFilter6}&filter6=${req.query.filter6}`
    : '';
  const enlace7 = req.query.filter7
    ? `&tableFilter7=${req.query.tableFilter7}&filter7=${req.query.filter7}`
    : '';
  const enlace8 = req.query.filter8
    ? `&tableFilter8=${req.query.tableFilter8}&filter8=${req.query.filter8}`
    : '';

  const enlace9 = req.query.filter9
    ? `&tableFilter9=${req.query.tableFilter9}&filter9=${req.query.filter9}`
    : '';
  const enlace10 = req.query.filter10
    ? `&tableFilter10=${req.query.tableFilter10}&filter10=${req.query.filter10}`
    : '';
  const enlace11 = req.query.filter11
    ? `&tableFilter11=${req.query.tableFilter11}&filter11=${req.query.filter11}`
    : '';
  const enlace12 = req.query.filter12
    ? `&tableFilter12=${req.query.tableFilter12}&filter12=${req.query.filter12}`
    : '';
  const enlace13 = req.query.filter13
    ? `&tableFilter13=${req.query.tableFilter13}&filter13=${req.query.filter13}`
    : '';
  const enlace14 = req.query.filter14
    ? `&tableFilter14=${req.query.tableFilter14}&filter14=${req.query.filter14}`
    : '';
  const sql = `SELECT * FROM publications WHERE statusPost = 'activated' AND idPostAssociated = "" AND ${table} LIKE '${filter}' ${filter1} ${filter2} ${filter3} ${filter4} ${filter5} ${filter6} ${filter7} ${filter8} ${filter9} ${filter10} ${filter11} ${filter12} ${filter13} ${filter14}`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      if (req.query.offSet > 0) {
        offSet = req.query.offSet;
      }
      page = Math.ceil(results.length / limit);
      const newsql = `SELECT * FROM publications WHERE statusPost = 'activated' AND idPostAssociated = "" AND ${table} LIKE '${filter}' ${filter1} ${filter2} ${filter3} ${filter4} ${filter5} ${filter6} ${filter7} ${filter8} ${filter9} ${filter10} ${filter11} ${filter12} ${filter13} ${filter14} ORDER BY FIELD (idPlan,100300,100301,100302,100400) DESC, orden,numberViews DESC LIMIT ${offSet},${limit}`;
      conexion.query(newsql, (e, r) => {
        if (e) throw e;
        if (r.length > 0) {
          if (req.query.offSet) {
            next = parseInt(req.query.offSet) + limit;
            if (parseInt(req.query.offSet) + limit >= results.length) {
              nextUrl = null;
            } else {
              nextUrl = `https://backend.panamaparaninos.com/api/v1/publications/get-filter/?table=${table}&filter=${filter}${enlace1}${enlace2}${enlace3}${enlace4}${enlace5}${enlace6}${enlace7}${enlace8}${enlace9}${enlace10}${enlace11}${enlace12}${enlace13}${enlace14}&offSet=${next}`;
            }
          } else if (results.length > limit) {
            nextUrl = `https://backend.panamaparaninos.com/api/v1/publications/get-filter/?table=${table}&filter=${filter}${enlace1}${enlace2}${enlace3}${enlace4}${enlace5}${enlace6}${enlace7}${enlace8}${enlace9}${enlace10}${enlace11}${enlace12}${enlace13}${enlace14}&offSet=${limit}`;
          } else {
            nextUrl = null;
          }

          if (req.query.offSet) {
            previous = parseInt(req.query.offSet) - limit;
            if (parseInt(req.query.offSet) - limit < 0) {
              previousUrl = null;
            } else {
              previousUrl = `https://backend.panamaparaninos.com/api/v1/publications/get-filter/?table=${table}&filter=${filter}${enlace1}${enlace2}${enlace3}${enlace4}${enlace5}${enlace6}${enlace7}${enlace8}${enlace9}${enlace10}${enlace11}${enlace12}${enlace13}${enlace14}&offSet=${previous}`;
            }
          } else {
            previousUrl = null;
          }
          res.send({
            status: 'OK',
            numberResult: results.length,
            next: nextUrl,
            previous: previousUrl,
            pageNumber: page,
            data: r,
          });
        } else {
          res
            .status(200)
            .send({ status: 'NOT DATA', data: 'no results found' });
        }
      });
    } else {
      res.status(200).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getPublication = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM publications WHERE id = '${id}' ORDER BY orden,numberViews DESC`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getPublicationForName = (req, res) => {
  const { keyword } = req.body;
  const sql = `SELECT * FROM publications WHERE seoKeyword = '${keyword}' ORDER BY orden,numberViews DESC`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const multiConsultePublication = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM publications, customersUsers, plans WHERE publications.id = '${id}' AND customersUsers.idCustomer = publications.idUser AND plans.idplan = publications.idPlan ORDER BY orden,numberViews DESC`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const searchPost = (req, res) => {
  const { search } = req.body;
  const sql = `SELECT name AS 'label', id AS 'value' FROM publications WHERE statusPost = 'activated' AND name LIKE '%${search}%' ORDER BY orden,numberViews DESC LIMIT 10`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', numberResult: results.length, data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const similarPosts = (req, res) => {
  const { subCategiry, city, zone } = req.body;
  const sql = `SELECT * FROM publications WHERE idPlan >= 100300 AND idPlan <= 100400 AND idPostAssociated = '' AND subCategories LIKE '%${subCategiry}%' AND city = '${city}' ORDER BY FIELD (zone, '${zone}') DESC, numberViews DESC LIMIT 4`;
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const searchPublication = (req, res) => {
  const { search, limit, offSet } = req.body;
  const sql = `SELECT * FROM publications WHERE statusPost = 'activated' AND MATCH(name,metaTagKeywords) AGAINST ('${search}')`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      const newsql = `SELECT * FROM publications WHERE statusPost = 'activated' AND MATCH(name,metaTagKeywords) AGAINST ('${search}') LIMIT ${offSet},${limit}`;
      conexion.query(newsql, (e, r) => {
        if (e) throw e;
        if (r.length > 0) {
          res.send({
            status: 'OK',
            resultNumber: result.length,
            numberPages: Math.ceil(result.length / limit),
            data: r,
          });
        } else {
          res
            .status(401)
            .send({ status: 'NOT DATA', data: 'no results found' });
        }
      });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

module.exports = {
  searchPublication,
  createPublications,
  editPublications,
  deletePublications,
  getPublications,
  multiGetPublications,
  getPublication,
  multiConsultePublication,
  getPagePublications,
  getFilterPublications,
  getPublicationsForCustomer,
  getBranchPublications,
  getPublicationsBranch,
  multiGetPublicationsFilter,
  searchPost,
  getPublicationsAndPromotionalTag,
  similarPosts,
  getPublicationForName,
};
