const conexion = require('../../sql/conexion');

const createFiltersByService = async (req, res) => {
  const sql = 'INSERT INTO filtersByService SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Service Create!' });
  });
};

const deleteFiltersByService = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersByService WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Service Deleted' });
  });
};

const getFiltersByServices = (req, res) => {
  const sql = 'SELECT * FROM filtersByService';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersByServices = (req, res) => {
  const sql =
    'SELECT keyword AS `value`, name AS `label` FROM filtersByService';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersByService = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersByService WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersByService = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersByService SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Service updated' });
  });
};

module.exports = {
  createFiltersByService,
  deleteFiltersByService,
  getFiltersByService,
  getFiltersByServices,
  consulteFiltersByServices,
  updateFiltersByService,
};
