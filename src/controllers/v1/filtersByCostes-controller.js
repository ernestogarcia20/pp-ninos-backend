const conexion = require('../../sql/conexion');

const createFiltersByCoste = async (req, res) => {
  const sql = 'INSERT INTO filtersByCostes SET ?';
  const userObj = req.body;
  conexion.query(sql, userObj, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Coste Create!' });
  });
};

const deleteFiltersByCoste = (req, res) => {
  const { id } = req.body;
  const sql = `DELETE FROM filtersByCostes WHERE id = ${id}`;
  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Coste Deleted' });
  });
};

const getFiltersByCostes = (req, res) => {
  const sql = 'SELECT * FROM filtersByCostes';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const consulteFiltersByCostes = (req, res) => {
  const sql = 'SELECT keyword AS `value`, name AS `label` FROM filtersByCostes';
  conexion.query(sql, (error, results) => {
    if (error) throw error;
    if (results.length > 0) {
      res.send({ status: 'OK', data: results });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const getFiltersByCoste = (req, res) => {
  const { id } = req.body;
  const sql = `SELECT * FROM filtersByCostes WHERE id = ${id}`;
  conexion.query(sql, (error, result) => {
    if (error) throw error;
    if (result.length > 0) {
      res.send({ status: 'OK', data: result });
    } else {
      res.status(401).send({ status: 'NOT DATA', data: 'no results found' });
    }
  });
};

const updateFiltersByCoste = (req, res) => {
  const { id } = req.body;

  const name = `name='${req.body.name}', `;
  const keyword = `keyword='${req.body.keyword}'`;

  const sql = `UPDATE filtersByCostes SET ${name}${keyword} WHERE id = ${id}`;

  conexion.query(sql, (error) => {
    if (error) throw error;
    res.send({ status: 'OK', message: 'Filters By Coste updated' });
  });
};

module.exports = {
  createFiltersByCoste,
  deleteFiltersByCoste,
  getFiltersByCoste,
  getFiltersByCostes,
  consulteFiltersByCostes,
  updateFiltersByCoste,
};
